import 'dart:convert';

import 'package:test_task/models/post.dart';
import 'package:http/http.dart' as http;

class PostProvider {
  Future<List<Post>> getPhoto() async {
    final response = await http.get(
        'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0');

    if (response.statusCode == 200) {
      final List<dynamic> photoJson = json.decode(response.body);
      return photoJson.map((json) => Post.fromJson(json)).toList();
    } else {
      throw Exception('Error fetching photos');
    }
  }
}
