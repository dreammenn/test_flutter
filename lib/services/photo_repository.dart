import 'package:test_task/models/post.dart';
import 'package:test_task/services/photo_api_provider.dart';

class PostsRepository {
  PostProvider _photosProvider = PostProvider();
  Future<List<Post>> getAllPhotos() => _photosProvider.getPhoto();
}
