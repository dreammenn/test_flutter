import 'package:auto_size_text/auto_size_text.dart';
import 'package:test_task/bloc/post_bloc.dart';
import 'package:test_task/bloc/post_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task/helpers/colors.dart';

class PostsList extends StatelessWidget {
  Widget heder() {
    return Container(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        padding: EdgeInsets.symmetric(horizontal: 15),
        color: Color(CustomColor.lightBlue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(Icons.arrow_right, color: Colors.white),
                    Text(
                      'Тема дня',
                      style: TextStyle(fontSize: 14, color: Colors.white),
                    ),
                  ],
                ),
                IconButton(
                  icon: Icon(Icons.close, color: Colors.white),
                  onPressed: () {},
                ),
              ],
            ),
            AutoSizeText(
              'А как вы с любимым ласково называете друг друга?',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.white),
              maxLines: 2,
              minFontSize: 10,
            ),
            SizedBox(height: 30),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostBloc, PostState>(
      builder: (context, state) {
        if (state is PostLoadedState) {
          return ListView.builder(
            itemCount: state.loadedPost.length,
            itemBuilder: (context, index) => index == 0
                ? heder()
                : Container(
                    color: Colors.white,
                    margin: EdgeInsets.symmetric(vertical: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 10),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(width: 10),
                            CircleAvatar(
                              radius: 20,
                              backgroundImage: NetworkImage(state.loadedPost[index].urls.regular),
                            ),
                            SizedBox(width: 10),
                            Text(state.loadedPost[index].user.name,
                                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                            SizedBox(width: 20),
                            Container(
                              child: Text(
                                '1г 2м',
                                style: TextStyle(fontSize: 12, color: Colors.white),
                              ),
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.all(Radius.circular(40)),
                                border: Border.all(width: 2, color: Colors.red, style: BorderStyle.solid),
                              ),
                            ),
                            SizedBox(width: 10),
                            Container(
                              child: Text(
                                '2н',
                                style: TextStyle(fontSize: 12, color: Colors.white),
                              ),
                              decoration: BoxDecoration(
                                color: Color(CustomColor.lightBlue),
                                borderRadius: BorderRadius.all(Radius.circular(40)),
                                border:
                                    Border.all(width: 2, color: Color(CustomColor.lightBlue), style: BorderStyle.solid),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Image(
                          image: NetworkImage(state.loadedPost[index].urls.regular),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Погодки!', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                              SizedBox(height: 10),
                              AutoSizeText(
                                // state.loadedPost[index].altDescription.toString(),
                                'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века...',
                                style: TextStyle(fontSize: 14),
                                maxLines: 4,
                              ),
                              SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('15 мая 2018', style: TextStyle(fontSize: 14, color: Colors.grey)),
                                  Row(
                                    children: [
                                      Icon(Icons.arrow_right, color: Colors.grey),
                                      Text('От рождения до года', style: TextStyle(fontSize: 14, color: Colors.grey)),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  IconButton(
                                    icon: Icon(Icons.favorite_border, color: Colors.grey),
                                    onPressed: () {},
                                  ),
                                  // SizedBox(width: 5),
                                  Text('24'),
                                  SizedBox(width: 15),
                                  IconButton(
                                    icon: Icon(Icons.comment, color: Colors.grey),
                                    onPressed: () {},
                                  ),
                                  SizedBox(width: 5),
                                  Text('14'),
                                  SizedBox(width: 15),
                                  IconButton(
                                    icon: Icon(Icons.share, color: Colors.grey),
                                    onPressed: () {},
                                  ),
                                ],
                              ),
                              IconButton(
                                icon: Icon(Icons.star_border, color: Colors.grey),
                                onPressed: () {},
                              ),
                              // SizedBox(width: 15),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
          );
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
