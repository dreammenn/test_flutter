import 'package:test_task/bloc/post_bloc.dart';
import 'package:test_task/bloc/post_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ActionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PostBloc postBloc = BlocProvider.of<PostBloc>(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        RaisedButton(
          child: Text('Load'),
          onPressed: () {
            postBloc.add(PostLoadEvent());
          },
          color: Colors.lightGreen,
        ),
      ],
    );
  }
}
