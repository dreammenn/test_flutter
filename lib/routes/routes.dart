import 'package:flutter/material.dart';
import 'package:test_task/pages/home_page.dart';
import 'package:test_task/routes/screen_names.dart';

Map<String, WidgetBuilder> generateRoute() => {
      ScreenNames.homePage: (context) => HomePage(),
    };
