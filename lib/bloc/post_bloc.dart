import 'package:test_task/bloc/post_event.dart';
import 'package:test_task/bloc/post_state.dart';
import 'package:test_task/models/post.dart';
import 'package:test_task/services/photo_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final PostsRepository postsRepository;
  PostBloc({this.postsRepository}) : super(PostEmptyState());

  @override
  Stream<PostState> mapEventToState(PostEvent event) async* {
    if (event is PostLoadEvent) {
      yield PostLoadingState();
      try {
        final List<Post> _loadedPostList = await postsRepository.getAllPhotos();
        yield PostLoadedState(loadedPost: _loadedPostList);
      } catch (_) {
        yield PostErrorState();
      }
    } else if (event is PostClearEvent) {
      yield PostEmptyState();
    }
  }
}
