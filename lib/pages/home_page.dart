import 'package:auto_size_text/auto_size_text.dart';
import 'package:test_task/bloc/post_bloc.dart';
import 'package:test_task/bloc/post_event.dart';
import 'package:test_task/bloc/post_state.dart';
import 'package:test_task/helpers/colors.dart';
import 'package:test_task/services/photo_repository.dart';
import 'package:test_task/widgets/action_buttons.dart';
import 'package:test_task/widgets/posts_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  final postRepository = PostsRepository();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PostBloc>(
      create: (context) => PostBloc(postsRepository: postRepository),
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Color(CustomColor.lightGrey),
          appBar: AppBar(
            leading: IconButton(icon: Icon(Icons.menu), onPressed: () {}),
            title: AutoSizeText(
              'Грудное вскармливание',
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              minFontSize: 10,
              maxLines: 1,
            ),
            actions: [
              IconButton(icon: Icon(Icons.navigation), onPressed: () {}),
              IconButton(icon: Icon(Icons.search), onPressed: () {}),
            ],
            bottom: TabBar(
              indicatorColor: Color(CustomColor.lightBlue),
              indicatorWeight: 5,
              unselectedLabelStyle: TextStyle(color: Color(CustomColor.lightGrey)),
              labelStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              tabs: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: AutoSizeText(
                    'Новые',
                    maxLines: 1,
                    minFontSize: 10,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: AutoSizeText(
                    'Популярные',
                    maxLines: 1,
                    minFontSize: 10,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: AutoSizeText(
                    'Подписки',
                    maxLines: 1,
                    minFontSize: 10,
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            physics: ScrollPhysics(),
            children: <Widget>[
              for (int i = 0; i < 3; i++)
                BlocBuilder<PostBloc, PostState>(builder: (context, state) {
                  if (state is PostEmptyState) {
                    return Center(
                      child: ActionButton(),
                    );
                  }

                  if (state is PostLoadingState) {
                    return Center(child: CircularProgressIndicator());
                  }

                  if (state is PostLoadedState) {
                    return PostsList();
                  }

                  if (state is PostErrorState) {
                    return Center(
                      child: Text('Error fetching posts', style: Theme.of(context).textTheme.caption),
                    );
                  }

                  return Center(child: CircularProgressIndicator());
                }),
            ],
          ),
        ),
      ),
    );
  }
}
