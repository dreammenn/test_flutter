import 'package:flutter/material.dart';
import 'package:test_task/routes/routes.dart';
import 'package:test_task/routes/screen_names.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: ScreenNames.homePage,
      routes: generateRoute(),
      theme: ThemeData(
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        primaryColor: Colors.white,
        textTheme: TextTheme(
          caption: TextStyle(
            fontSize: 20,
            color: Colors.black,
            decoration: TextDecoration.none,
          ),
        ),
      ),
    );
  }
}
